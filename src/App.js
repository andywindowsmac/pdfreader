import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Viewer from './reader/generic/web';

class App extends Component {
  shouldComponentUpdate() {
    return false;
  }
// pdfFile - send this prop 
  render() {
    return (
      <div className="App">
        <Viewer 
        />
      </div>
    );
  }
}

export default App;
